import { TFieldTypes } from "./types";

export interface IHiddenField {
    readonly fieldType: TFieldTypes | string;
    readonly fieldValue: string | undefined;
    readonly fieldQueryStringParameter: string | undefined;
    readonly fieldCookie: string | undefined;
    readonly fieldLocalStorage: string | undefined;
    readonly fieldSessionStorage: string | undefined;
    readonly fieldVariable: string | undefined;
}

export type THiddenFieldStringConditionMode =
    | "equals"
    | "not-equals"
    | "contains"
    | "not-contains"
    | "starts"
    | "ends"
    | "defined"
    | "undefined"
    | "regex";

export interface IHiddenFieldStringCondition {
    readonly mode: THiddenFieldStringConditionMode;
    readonly value?: string;
    readonly ignoreCase?: boolean;
    readonly regex?: string;
    readonly invert?: boolean;
}

export type THiddenFieldNumberConditionMode =
    | "equal"
    | "not-equal"
    | "below"
    | "above"
    | "between"
    | "not-between";

export interface IHiddenFieldNumberCondition {
    readonly mode: THiddenFieldNumberConditionMode;
    readonly value?: number | string;
    readonly to?: number | string;
}

export interface IHiddenFieldOrientationCondition {
    readonly orientation: "landscape" | "portrait";
}

export type THiddenFieldDateConditionMode =
    | "equal"
    | "not-equal"
    | "before"
    | "after"
    | "between"
    | "not-between";

export interface IHiddenFieldDateCondition {
    readonly mode: THiddenFieldDateConditionMode;
    readonly value?: number | string;
    readonly to?: number | string;
}
