/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    L10n,
    Slots,
    affects,
    definition,
    editor,
    isNumberFinite,
    isString,
    lookupVariable,
    pgettext,
    populateVariables,
    tripetto,
} from "tripetto";
import {
    IHiddenFieldNumberCondition,
    THiddenFieldNumberConditionMode,
} from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-number.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:number`,
    alias: `${PACKAGE_NAME}-number`,
    version: PACKAGE_VERSION,
    icon: ICON,
    autoOpen: true,
    get label() {
        return pgettext("block:hidden-field", "Compare value");
    },
})
export class HiddenFieldNumberCondition
    extends ConditionBlock
    implements IHiddenFieldNumberCondition
{
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    mode: THiddenFieldNumberConditionMode = "equal";

    @definition
    @affects("#name")
    value?: number | string;

    @definition
    @affects("#name")
    to?: number | string;

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        const slot = this.slot;

        if (slot instanceof Slots.Number) {
            const value = this.parse(slot, this.value);

            switch (this.mode) {
                case "between":
                    return `${value} ≤ @${slot.id} ≤ ${this.parse(
                        slot,
                        this.to
                    )}`;
                case "not-between":
                    return `@${slot.id} < ${value} ${pgettext(
                        "block:hidden-field",
                        "or"
                    )} @${slot.id} > ${this.parse(slot, this.to)}`;
                case "not-equal":
                    return `@${slot.id} \u2260 ${value}`;
                case "above":
                case "below":
                case "equal":
                    return `@${slot.id} ${
                        this.mode === "above"
                            ? ">"
                            : this.mode === "below"
                            ? "<"
                            : "="
                    } ${value}`;
            }
        }

        return this.type.label;
    }

    get title() {
        return this.node?.label;
    }

    private parse(
        slot: Slots.Number,
        value: number | string | undefined
    ): string {
        if (isNumberFinite(value)) {
            return slot.toString(value, (n) => L10n.locale.number(n));
        } else if (
            isString(value) &&
            value &&
            lookupVariable(this, value)?.label
        ) {
            return "@" + value;
        }

        return "\\_\\_";
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:hidden-field", "Compare mode"),
            controls: [
                new Forms.Radiobutton<THiddenFieldNumberConditionMode>(
                    [
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Number is equal to"
                            ),
                            value: "equal",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Number is not equal to"
                            ),
                            value: "not-equal",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Number is lower than"
                            ),
                            value: "below",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Number is higher than"
                            ),
                            value: "above",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Number is between"
                            ),
                            value: "between",
                        },
                        {
                            label: pgettext(
                                "block:hidden-field",
                                "Number is not between"
                            ),
                            value: "not-between",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "equal")
                ).on(
                    (
                        mode: Forms.Radiobutton<THiddenFieldNumberConditionMode>
                    ) => {
                        to.visible(
                            mode.value === "between" ||
                                mode.value === "not-between"
                        );

                        switch (mode.value) {
                            case "equal":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If number equals"
                                );
                                break;
                            case "not-equal":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If number not equals"
                                );
                                break;
                            case "below":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If number is lower than"
                                );
                                break;
                            case "above":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If number is higher than"
                                );
                                break;
                            case "between":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If number is between"
                                );
                                break;
                            case "not-between":
                                from.title = pgettext(
                                    "block:hidden-field",
                                    "If number is not between"
                                );
                                break;
                        }
                    }
                ),
            ],
        });

        const addCondition = (
            property: "value" | "to",
            title: string,
            visible: boolean
        ) => {
            const value = this[property];
            const numberControl = new Forms.Numeric(
                isNumberFinite(value) ? value : 0
            )
                .label(pgettext("block:hidden-field", "Use fixed number"))
                .autoFocus(property === "value")
                .escape(this.editor.close)
                .enter(
                    () =>
                        ((this.mode !== "between" &&
                            this.mode !== "not-between") ||
                            property === "to") &&
                        this.editor.close()
                )
                .on((input) => {
                    if (input.isFormVisible && input.isObservable) {
                        this[property] = input.value;
                    }
                });

            const variables = populateVariables(
                this,
                (slot) =>
                    slot instanceof Slots.Number ||
                    slot instanceof Slots.Numeric,
                isString(value) ? value : undefined,
                false,
                this.slot?.id
            );
            const variableControl = new Forms.Dropdown(
                variables,
                isString(value) ? value : ""
            )
                .label(pgettext("block:hidden-field", "Use value of"))
                .width("full")
                .on((variable) => {
                    if (variable.isFormVisible && variable.isObservable) {
                        this[property] = variable.value || "";
                    }
                });

            return this.editor
                .form({
                    title,
                    controls: [
                        new Forms.Radiobutton<"number" | "variable">(
                            [
                                {
                                    label: pgettext(
                                        "block:hidden-field",
                                        "Number"
                                    ),
                                    value: "number",
                                },
                                {
                                    label: pgettext(
                                        "block:hidden-field",
                                        "Value"
                                    ),
                                    value: "variable",
                                    disabled: variables.length === 0,
                                },
                            ],
                            isString(value) ? "variable" : "number"
                        ).on((type) => {
                            numberControl.visible(type.value === "number");
                            variableControl.visible(type.value === "variable");

                            if (numberControl.isObservable) {
                                numberControl.focus();
                            }
                        }),
                        numberControl,
                        variableControl,
                    ],
                })
                .visible(visible);
        };

        const from = addCondition(
            "value",
            pgettext("block:hidden-field", "If number equals"),
            true
        );
        const to = addCondition(
            "to",
            pgettext("block:hidden-field", "And"),
            this.mode === "between" || this.mode === "not-between"
        );
    }
}
