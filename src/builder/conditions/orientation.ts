/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { IHiddenFieldOrientationCondition } from "../../runner/interface";

/** Assets */
import ICON from "../../../assets/condition-orientation.svg";

@tripetto({
    type: "condition",
    context: PACKAGE_NAME,
    identifier: `${PACKAGE_NAME}:orientation`,
    alias: `${PACKAGE_NAME}-orientation`,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:hidden-field", "Orientation");
    },
})
export class HiddenFieldOrientationCondition
    extends ConditionBlock
    implements IHiddenFieldOrientationCondition
{
    readonly allowMarkdown = true;

    @definition
    @affects("#name")
    orientation: "landscape" | "portrait" = "landscape";

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            return `@${this.slot.id} = ${
                this.orientation === "landscape"
                    ? pgettext("block:hidden-field", "Landscape")
                    : pgettext("block:hidden-field", "Portrait")
            }`;
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            controls: [
                new Forms.Radiobutton<"landscape" | "portrait">(
                    [
                        {
                            label: pgettext("block:hidden-field", "Landscape"),
                            value: "landscape",
                        },
                        {
                            label: pgettext("block:hidden-field", "Portrait"),
                            value: "portrait",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "orientation", "landscape")
                ),
            ],
        });
    }
}
